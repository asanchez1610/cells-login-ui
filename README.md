# @capacitation-component/cells-login-ui

## Package info

### Package installation

Installation using NPM

```bash
npm install @capacitation-component/cells-login-ui
```



## CellsLoginUi (Class) cells-login-ui (Custom Element) 

### Extends from

LitElement (lit-element package)

### Description

![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
  <cells-login-ui></cells-login-ui>
```

### Properties

- **documentNumber** (attribute: documentNumber): string = ""
    This property represents the client's document number
- **documentType** (attribute: documentType): string = ""
    This property represents the client's document type
- **itemsDocuments** (attribute: itemsDocuments): array = []
    this property is set for document type data
- **password** (attribute: password): string = ""
    This property represents the access password
- **titleLogin** (attribute: titleLogin): string = "Login App"
    This property is assigned to the form login

### Methods

- **login**
- **onChangeInput** ➞ event
    Este metodo permite setear los valores al ovjeto usuario
    - **name**: string
        key del objeto usuario
    - **{ target }**
- **resetForm**

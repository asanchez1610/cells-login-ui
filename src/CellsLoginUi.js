import { LitElement, html } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './cells-login-ui.css.js';
import '@bbva-web-components/bbva-web-form-select/bbva-web-form-select';
import '@bbva-web-components/bbva-web-form-password/bbva-web-form-password';
import '@bbva-web-components/bbva-web-button-default/bbva-web-button-default';
import '@bbva-web-components/bbva-web-form-text/bbva-web-form-text';

/**
 * ![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)
 *
 * This component ...
 *
 * Example:
 *
 * ```html
 *   <cells-login-ui></cells-login-ui>
 * ```
 */
export class CellsLoginUi extends LitElement {
  static get properties() {
    return {
      /**
       * This property is assigned to the form login
       */
      titleLogin: String,
      /**
       * This property represents the client's document type
       */
      documentType: String,
      /**
       * This property represents the client's document number
       */
      documentNumber: String,
      /**
       * This property represents the access password
       */
      password: String,
      /**
       * this property is set for document type data
       */
      itemsDocuments: Array,
    };
  }

  constructor() {
    super();
    this.titleLogin = 'Login App';
    this.documentType = '';
    this.password = '';
    this.documentNumber = '';
    this.itemsDocuments = [];
  }

  static get styles() {
    return [styles, getComponentSharedStyles('cells-login-ui-shared-styles')];
  }

  /**
   * Este metodo permite setear los valores al ovjeto usuario
   * @argument {String} name key del objeto usuario
   * @returns {Event} target el valor del input para el objeto usuario
   */
  onChangeInput(name, { target }) {
    console.log('name', target.value);
    this[name] = target.value;
  }

  login() {
    const user = {
      userId: `${this.documentType}${this.documentNumber}`,
      password: this.password
    };
    console.log('login', user);
    this.dispatchEvent(
      new CustomEvent('on-login-evt', { 
        detail: user,
        bubbles: true, 
        composed: true
      })
    );

  }

  resetForm() {
    console.log('resetForm');
    const inputs = this.shadowRoot.querySelectorAll('bbva-web-form-select, bbva-web-form-password, bbva-web-form-text');
    inputs.forEach(input => {
      input.value = '';
    });
  }

  render() {
    return html`
      <main class="login-container">
        <div class="login-container__form">
          <h2 class="title-form">${this.titleLogin}</h2>
          <div class="form-body">
            <bbva-web-form-select
              label="Tipo de Documento"
              name="documentType"
              @change="${(evt) => this.onChangeInput('documentType', evt)}"
            >
              ${this.itemsDocuments.map(
                (item) => html`
                  <bbva-web-form-option value="${item.value}"
                    >${item.name}</bbva-web-form-option
                  >
                `
              )}
            </bbva-web-form-select>

            <bbva-web-form-text
              label="Número de documento"
              name="documentNumber"
              @input="${(evt) => this.onChangeInput('documentNumber', evt)}"
            ></bbva-web-form-text>

            <bbva-web-form-password
              name="password"
              label="Contraseña"
              @input="${(evt) => this.onChangeInput('password', evt)}"
            >
            </bbva-web-form-password>

            <bbva-web-button-default @click="${this.login}"
              >Acceder</bbva-web-button-default
            >
          </div>
        </div>
      </main>
    `;
  }
}
